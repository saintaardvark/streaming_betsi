#!/usr/bin/env python3

import logging
from pathlib import Path
import tensorflow as tf
from tensorflow import keras

from betsi import models, preprocessors, predictors


project_dir = Path(__file__).resolve().parents[2]
# TODO: Use pathlib for this
model_dir = f"{project_dir}/models"
DEFAULT_MODEL_FILE = f"{model_dir}/original_notebook_model"


def load_model(model_file=DEFAULT_MODEL_FILE):
    """
    Load the specified model
    """
    print(f"Trying to load {model_file}...")
    model = tf.keras.models.load_model(model_file)
    return model

def main():
    """
    Main entry point
    """
    # 2021-05-29 16:04:57.368337: W
    # tensorflow/stream_executor/platform/default/dso_loader.cc:64]
    # Could not load dynamic library 'libcudart.so.11.0'; dlerror:
    # libcudart.so.11.0: cannot open shared object file: No such file
    # or directory
    #
    # Just a warning, and there may not be a good way to suppress it.
    # https://stackoverflow.com/questions/59823283/could-not-load-dynamic-library-cudart64-101-dll-on-tensorflow-cpu-only-install
    print("Hello, world!  Let's load the model:")
    model = load_model()
    model.summary()

if __name__ == "__main__":
    main()
